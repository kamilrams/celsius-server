<?php
class MeasureRepository
{
    private $pdo;

    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function createNew(int $temperature) {
        try {
            $date = new \DateTime();
            $dateStr = $date->format('Y-m-d H:i:s');

            $stmt = $this->pdo->prepare("INSERT INTO measures(temperature, date) VALUES (:temperature, :date)");
            $stmt->bindParam(":temperature", $temperature, PDO::PARAM_INT);
            $stmt->bindParam(":date", $dateStr, PDO::PARAM_STR);

            $stmt->execute();

            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    public function getAll() {
		$measures = [];
        $stmt = $this->pdo->query("SELECT rowid AS id, temperature, date FROM measures");
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($results as $key => $measure) {
			$measure = [
				'id' => intval($measure['id']),
				'temperature' => intval($measure['temperature']),
				'date' => $measure['date']
			];
			
			$measures[] = $measure;
		}
		
		
        return $measures;
    }

}
