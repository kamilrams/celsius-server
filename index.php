<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'MeasureRepository.php';

date_default_timezone_set("Europe/Warsaw");

$pdo = new PDO('sqlite:database.sqlite3');
$repository = new MeasureRepository($pdo);

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*');
});

$app->get('/', function (Request $request, Response $response) use ($repository) {
    $result = $repository->getAll();

    return $response->withJson($result);
});

$app->post('/measure/{temperature}', function (Request $request, Response $response, array $args) use ($repository) {
    $temperature = $args['temperature'];
    
    $result = $repository->createNew($temperature);

    return $response->withJson([
        'created' => $result
    ], 201);
});
$app->run();
